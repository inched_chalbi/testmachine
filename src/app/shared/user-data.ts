
export class UserData {
    id: number ;
    name: string | undefined;
    email: string | undefined;
    gender: string | undefined;
}
