import { TestBed } from '@angular/core/testing';

import { MachineStockService } from './machine-stock.service';

describe('MachineStockService', () => {
  let service: MachineStockService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MachineStockService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
