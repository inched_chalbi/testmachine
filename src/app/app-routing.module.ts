import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddUserComponent } from './add-user/add-user.component';
import { ListUserComponent } from './list-user/list-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';

const routes: Routes = [
{path: 'add-user', component: AddUserComponent},
{path: 'list-user', component: ListUserComponent},
{path: 'delete-user', component: DeleteUserComponent},
{path: 'edit-user', component: EditUserComponent},
{path: 'dialog-box', component: DialogBoxComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
