import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MachineStockService } from '../shared/machine-stock.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {DialogBoxComponent } from '../dialog-box/dialog-box.component';
import { UserData } from '../shared/user-data';



@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})

export class ListUserComponent implements OnInit {

  @ViewChild('studentForm', { static: false })

  dataSource: any ;
  length = 0 ;
  pageSize = 0;
  pageSizeOptions = [5, 10, 25, 100];
  displayedColumns: string[] = ['id', 'email', 'name', 'gender'];
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  isEditMode = false;
  usersData: UserData = new UserData();
  userForm = new FormGroup({
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(2)
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.minLength(2)
    ]),
    gender: new FormControl('', [
      Validators.required,
      Validators.minLength(2)
    ]),
  });

  constructor(private httpDataService: MachineStockService , public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getAllUsers();
  }

  get fval(): any{
    return this.userForm.controls;
  }

  getAllUsers(): void{
    this.httpDataService.getList().subscribe((response: any) => {
      console.log(response.data);
      this.length = response.meta.pagination.total;
      this.pageSize = response.meta.pagination.pages;
      this.pageSizeOptions = response.meta.pagination.page;
      this.dataSource = new MatTableDataSource(response.data);
    });
  }

  editItem(element: any): void {
    this.isEditMode = true;
  }

  cancelEdit(): void {
    this.isEditMode = false;
  }

  deleteItem(id: number): void {
    this.httpDataService.deleteItem(id).subscribe((response: any) => {

      // Approach #1 to update datatable data on local itself without fetching new data from server
      this.dataSource.data = this.dataSource.data.filter((o: any) => {
        return o.id !== id ? o : false;
      });
    });
  }

  addStudent(): void {
    this.httpDataService.createItem(this.usersData.name).subscribe((response: any) => {
      this.dataSource.data.push({ ...response });
      this.dataSource = this.dataSource.data.map((o: any) => {
        return o;
      });
    });
  }

  updateStudent(): void {
    this.httpDataService.updateItem(this.usersData.id, this.usersData).subscribe((response: any) => {

      // Approach #1 to update datatable data on local itself without fetching new data from server
      this.dataSource.data = this.dataSource.data.map((o: any) => {
        if (o.id === response.id) {
          o = response;
        }
        return o;
      });
    });
  }

  onSubmit(): void {
    if (this.userForm.valid) {
      if (this.isEditMode) {
        this.updateStudent();
      }
      else {
        this.addStudent();
      }
    } else {
      console.log('Enter valid data!');
    }
  }

  openDialog(action: any, obj: any): void {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data: obj
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if(result.event === 'Add'){
        this.addRowData(result.data);
      }else if (result.event === 'Update'){
        this.updateRowData(result.data);
      }else if (result.event === 'Delete'){
        this.deleteRowData(result.data);
      }
    });
  }

  addRowData(rowObj: any){
    const date = new Date();
    this.dataSource.push({
      id: date.getTime(),
      name: this.fval.name.value,
      email: this.fval.email.value,
      gender: this.fval.gender.value
    });
  }
  updateRowData(rowObj: any): any{
    this.dataSource = this.dataSource.filter((value: any, key: any)=>{
      if (value.id === rowObj.id){
        value.name = rowObj.name;
      }
      return true;
    });
  }
  deleteRowData(rowObj: any): any{
    this.dataSource = this.dataSource.filter((value: any, key: any) => {
      return value.id !== rowObj.id;
    });
  }
}
